rm -rf /opt/ANDRAX/exe2hex

mkdir /opt/ANDRAX/exe2hex

cp -Rf exe2hex.py /opt/ANDRAX/exe2hex/

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
